<?php

namespace App\Repository;

use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Location>
 *
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Location::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findByCity($country, $city): ?Location
    {
        $qb = $this->createQueryBuilder('l');
        $qb->where('l.country = :country')
            ->setParameter('country', $country)
            ->andWhere('l.city < :city')
            ->setParameter('city', $city);


        $query = $qb->getQuery();
        $result = $query->getOneOrNullResult();
        return $result;
    }
}
